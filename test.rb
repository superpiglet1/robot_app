require "test/unit"
require_relative "./robot/app"

class TestRobot < Test::Unit::TestCase

  def test_operation_command
    robot = App.new
    # Test place method
    robot.operation_command('PLACE 0,0,NORTH')
    assert_equal("0,0,NORTH", robot.operation_command('REPORT'))
    # Test move method on y axis
    robot.operation_command('MOVE')
    assert_equal("0,1,NORTH", robot.operation_command('REPORT'))
    # Test trun right method
    robot.operation_command('RIGHT')
    assert_equal("0,1,EAST", robot.operation_command('REPORT'))
    # Test move method on x axis
    robot.operation_command('MOVE')
    assert_equal("1,1,EAST", robot.operation_command('REPORT'))
    # Test invalid placement
    robot.operation_command('PLACE 6,6,SOUTH')
    assert_equal("1,1,EAST", robot.operation_command('REPORT'))
    # Test invalid movement
    robot.operation_command('PLACE 0,0,WEST')
    robot.operation_command('MOVE')
    assert_equal("0,0,WEST", robot.operation_command('REPORT'))
  end
end
