class Table
  attr_reader :width, :height
  def initialize(width: 5, height: 5)
    @width = width
    @height = height
  end

  def out_of_bound?(x, y)
    x > width || y > height || x < 0 || y < 0
  end
end
